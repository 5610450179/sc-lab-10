import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class GUI2 extends JFrame{
	
	private JPanel panel = new JPanel();
	private JPanel cPanel = new JPanel();
	private JButton bt = new JButton("Select");
	private JRadioButton btR = new JRadioButton("Red");
	private JRadioButton btG = new JRadioButton("Green");
	private JRadioButton btB = new JRadioButton("Blue");
	private ButtonGroup group = new ButtonGroup();
	
	public GUI2(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("Button");
		setBounds(500,100,300,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		group.add(btR);group.add(btG);group.add(btB);
		
		panel.add(btR); panel.add(btG); panel.add(btB);
		panel.add(bt);
		
		setLayout(new BorderLayout());
		
		add(cPanel,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	public JButton getButton(){
		return bt;
	}
	public JRadioButton getRed(){
		return btR;
	}
	public JRadioButton getGreen(){
		return btG;
	}
	public JRadioButton getBlue(){
		return btB;
	}
	public JPanel getColorPanel(){
		return cPanel;
	}

}
