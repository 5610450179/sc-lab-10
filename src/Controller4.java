import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller4 {

	private GUI4 view;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller4();

	}
	
	public Controller4(){
		view = new GUI4();
		setListener();
	}
	
	public void setListener(){
		//button
		view.getButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String select = (String) view.getCombo().getSelectedItem();
				if(select.equals("Red"))view.getColorPanel().setBackground(Color.RED);
				else if(select.equals("Green"))view.getColorPanel().setBackground(Color.GREEN);
				else view.getColorPanel().setBackground(Color.BLUE);
			}
		});

	}

}
