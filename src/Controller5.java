import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller5 {

	private GUI5 view;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller5();

	}
	
	public Controller5(){
		view = new GUI5();
		setListener();
	}
	
	public void setListener(){
		//red
		view.getRed().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getColorPanel().setBackground(Color.RED);
			}
		});
		//green
		view.getGreen().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getColorPanel().setBackground(Color.GREEN);
			}
		});
		//blue
		view.getBlue().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getColorPanel().setBackground(Color.BLUE);
			}
		});
	}

}
