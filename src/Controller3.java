import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller3 {

	private GUI3 view;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller3();

	}
	
	public Controller3(){
		view = new GUI3();
		setListener();
	}
	
	public void setListener(){
		//red
view.getButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean red = view.getRed().isSelected();
				boolean green = view.getGreen().isSelected();
				boolean blue= view.getBlue().isSelected();
				if(red & !green & !blue)view.getColorPanel().setBackground(Color.RED);
				else if(!red & green & !blue)view.getColorPanel().setBackground(Color.GREEN);
				else if(!red & !green & blue)view.getColorPanel().setBackground(Color.BLUE);
				else if(red & green & !blue)view.getColorPanel().setBackground(Color.YELLOW);
				else if(red & !green & blue)view.getColorPanel().setBackground(Color.PINK);
				else if(!red & green & blue)view.getColorPanel().setBackground(Color.decode("#0099FF"));
				else view.getColorPanel().setBackground(Color.WHITE);
			}
		});
	}

}
