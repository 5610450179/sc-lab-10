import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller7 {

	private GUI7 view;
	private BankAccount bank;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller7();

	}
	
	public Controller7(){
		bank = new BankAccount();
		view = new GUI7();
		setListener();
	}
	
	public void setListener(){
		//deposite
		view.getDeposit().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int amount = Integer.parseInt(view.getTxT().getText());
				bank.deposit(amount);
				view.getArea().setText(view.getArea().getText()+"\n"+"Deposit: "+amount+"\nBalance: "+bank.getBalance());
				view.getTxT().setText("0");
			}
		});
		//withdraw
		view.getWithdraw().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int amount = Integer.parseInt(view.getTxT().getText());
				bank.withdraw(amount);
				view.getArea().setText(view.getArea().getText()+"\n"+"Withdraw: "+amount+"\nBalance: "+bank.getBalance());
				view.getTxT().setText("0");
			}
		});
		
	
	}

}
