import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller2 {

	private GUI2 view;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller2();

	}
	
	public Controller2(){
		view = new GUI2();
		setListener();
	}
	
	public void setListener(){
		//button
		view.getButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(view.getRed().isSelected())view.getColorPanel().setBackground(Color.RED);
				else if(view.getGreen().isSelected())view.getColorPanel().setBackground(Color.GREEN);
				else view.getColorPanel().setBackground(Color.BLUE);
			}
		});

	}

}
