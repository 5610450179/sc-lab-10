import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class GUI6 extends JFrame{
	
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel(new BorderLayout());
	private JButton deposit = new JButton("Deposit");
	private JButton withdraw = new JButton("Withdraw");
	private JTextField txt = new JTextField("0");
	private JLabel lb = new JLabel("Balance: 0");
	
	public GUI6(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("BankAccount");
		setBounds(500,100,400,150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel1.add(deposit); panel1.add(withdraw);
		panel2.add(lb);
		panel3.add(txt,BorderLayout.CENTER);
		panel3.add(new JPanel(),BorderLayout.EAST);
		panel3.add(new JPanel(),BorderLayout.WEST);
		panel3.add(new JPanel(),BorderLayout.NORTH);
		
		setLayout(new BorderLayout());
		add(panel3,BorderLayout.NORTH);
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	public JButton getDeposit(){
		return deposit;
	}
	public JButton getWithdraw(){
		return withdraw;
	}
	public JLabel getLabel(){
		return lb;
	}
	public JTextField getTxT(){
		return txt;
	}

}
