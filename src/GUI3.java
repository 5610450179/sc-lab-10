import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI3 extends JFrame{
	
	private JPanel panel = new JPanel();
	private JPanel cPanel = new JPanel();
	private JButton bt = new JButton("Select");
	private JCheckBox btR = new JCheckBox("Red");
	private JCheckBox btG = new JCheckBox("Green");
	private JCheckBox btB = new JCheckBox("Blue");
	
	public GUI3(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("Button");
		setBounds(500,100,300,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		panel.add(btR); panel.add(btG); panel.add(btB);
		panel.add(bt);
		
		setLayout(new BorderLayout());
		
		add(cPanel,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	public JButton getButton(){
		return bt;
	}
	public JCheckBox getRed(){
		return btR;
	}
	public JCheckBox getGreen(){
		return btG;
	}
	public JCheckBox getBlue(){
		return btB;
	}
	public JPanel getColorPanel(){
		return cPanel;
	}

}
