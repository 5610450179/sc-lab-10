import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GUI7 extends JFrame{
	
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel(new BorderLayout());
	private JPanel panel3 = new JPanel(new BorderLayout());
	private JButton deposit = new JButton("Deposit");
	private JButton withdraw = new JButton("Withdraw");
	private JTextField txt = new JTextField("0");
	private JTextArea area = new JTextArea("Balance: 0");
	private JScrollPane scroll = new JScrollPane();
	
	
	public GUI7(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("BankAccount");
		setBounds(500,100,400,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		panel1.add(deposit); panel1.add(withdraw);
		panel2.add(scroll);
		scroll.setViewportView(area);
		panel3.add(txt,BorderLayout.CENTER);
		panel3.add(new JPanel(),BorderLayout.EAST);
		panel3.add(new JPanel(),BorderLayout.WEST);
		panel3.add(new JPanel(),BorderLayout.NORTH);
		panel3.add(panel1,BorderLayout.SOUTH);
		
		setLayout(new BorderLayout());
		add(panel3,BorderLayout.NORTH);
		add(panel2,BorderLayout.CENTER);
		add(new JPanel(),BorderLayout.EAST);
		add(new JPanel(),BorderLayout.WEST);
		add(new JPanel(),BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	public JButton getDeposit(){
		return deposit;
	}
	public JButton getWithdraw(){
		return withdraw;
	}
	public JTextField getTxT(){
		return txt;
	}
	public JTextArea getArea(){
		return area;
	}

}
