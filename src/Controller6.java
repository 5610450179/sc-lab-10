import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller6 {

	private GUI6 view;
	private BankAccount bank;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller6();

	}
	
	public Controller6(){
		bank = new BankAccount();
		view = new GUI6();
		setListener();
	}
	
	public void setListener(){
		//deposite
		view.getDeposit().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int amount = Integer.parseInt(view.getTxT().getText());
				bank.deposit(amount);
				view.getLabel().setText(view.getLabel().getText().substring(0,8)+bank.getBalance());
				view.getTxT().setText("0");
			}
		});
		//withdraw
		view.getWithdraw().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int amount = Integer.parseInt(view.getTxT().getText());
				bank.withdraw(amount);
				view.getLabel().setText(view.getLabel().getText().substring(0,8)+bank.getBalance());
				view.getTxT().setText("0");
			}
		});
		
	
	}

}
