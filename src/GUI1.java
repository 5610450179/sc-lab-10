import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI1 extends JFrame{
	
	private JPanel panel = new JPanel();
	private JPanel cPanel = new JPanel();
	private JButton btR = new JButton("Red");
	private JButton btG = new JButton("Green");
	private JButton btB = new JButton("Blue");
	
	public GUI1(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("Button");
		setBounds(500,100,300,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.add(btR); panel.add(btG); panel.add(btB);
		
		setLayout(new BorderLayout());
		
		add(cPanel,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	public JButton getRed(){
		return btR;
	}
	public JButton getGreen(){
		return btG;
	}
	public JButton getBlue(){
		return btB;
	}
	public JPanel getColorPanel(){
		return cPanel;
	}

}
