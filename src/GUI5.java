import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class GUI5 extends JFrame{
	
	
	private JPanel cPanel = new JPanel();
	private JMenuBar bar = new JMenuBar();
	private JMenu menu = new JMenu("RGB");
	private JMenuItem btR = new JMenuItem("Red");
	private JMenuItem btG = new JMenuItem("Green");
	private JMenuItem btB = new JMenuItem("Blue");
	
	public GUI5(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("Button");
		setBounds(500,100,300,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menu.add(btR); menu.add(btG); menu.add(btB);
		bar.add(menu);
		
		setLayout(new BorderLayout());
		
		setJMenuBar(bar);
		add(cPanel,BorderLayout.CENTER);
		
		setVisible(true);
		
	}
	
	public JMenuItem getRed(){
		return btR;
	}
	public JMenuItem getGreen(){
		return btG;
	}
	public JMenuItem getBlue(){
		return btB;
	}
	public JPanel getColorPanel(){
		return cPanel;
	}

}
