import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller1 {

	private GUI1 view;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller1();

	}
	
	public Controller1(){
		view = new GUI1();
		setListener();
	}
	
	public void setListener(){
		//red
		view.getRed().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getColorPanel().setBackground(Color.RED);
			}
		});
		//green
		view.getGreen().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getColorPanel().setBackground(Color.GREEN);
			}
		});
		//blue
		view.getBlue().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getColorPanel().setBackground(Color.BLUE);
			}
		});
	}

}
