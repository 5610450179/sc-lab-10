import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class GUI4 extends JFrame{
	
	private JPanel panel = new JPanel();
	private JPanel cPanel = new JPanel();
	private JButton bt = new JButton("Select");
	private JComboBox combo;
	
	public GUI4(){
		createFrame();
	}
	
	private void createFrame(){
		setTitle("Button");
		setBounds(500,100,300,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		String[] list = {"Red","Green","Blue"};
		combo = new JComboBox(list);
		panel.add(combo); panel.add(bt);
		
		setLayout(new BorderLayout());
		
		add(cPanel,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
		
		setVisible(true);
		
	}
	
	public JButton getButton(){
		return bt;
	}
	public JComboBox getCombo(){
		return combo;
	}
	public JPanel getColorPanel(){
		return cPanel;
	}

}
